Metadata-Version: 1.0
Name: bibliograph.core
Version: 1.0.3
Summary: Core definitions of bibliograph packages
Home-page: http://pypi.python.org/pypi/bibliograph.core
Author: Tom Gross
Author-email: itconsense@gmail.com
License: ZPL 2.1
Description: bibliograph.core
        ================
        
        Overview
        --------
        
        Core definitions of bibliograph packages
        
        Here all bits and pieces are defined which are commonly used by the
        packagages sharing the `bibliograph` namespace. We provide some interfaces
        here:
        
        IBibliographicReference is an interface for a single content object with a given
        schema which can be rendered as a bibliographic entry (bibtex, endnote,
        ris, etc.).
        
        >>> from bibliograph.core import interfaces
        >>> 'IBibliographicReference' in dir(interfaces)
        True
        
        IBibliographyExport is a marker for a container directly
        containing single exportable IBibliographicReference objects.
        
        >>> 'IBibliographyExport' in dir(interfaces)
        True
        
        Another part of the package are utility methods and a collection of encodings
        used within python and latex including a mapping.
        
        A utility method `bin_search` is included. It acts like the `which`-command on
        posix systems. It returns the full path of an executeable command, if it is
        found in the PATH environment variable.
        
        You may overload the PATH environment variable with another environment
        variable: BIBUTILS_PATH. Executeables in this location will be found as well.
        
        Resources
        ---------
        
        - Homepage: http://pypi.python.org/pypi/bibliograph.core
        - Code repository: http://svn.plone.org/svn/collective/bibliograph.core/
        
        Contributors
        ------------
        
        - Tom Gross, itconsense@gmail.com, Author
        - Raphael Ritz, r.ritz@biologie.hu-berlin.de, Renderers
        - Andreas Jung, info@zopyx.com, bugfixes
        
        Change history
        --------------
        
        1.0.3 (2010-07-24)
        ==================
        * fixed output encoding for xml2bib conversion settings
        
        1.0.2 (2010-07-23)
        ==================
        * removed \mbox from \texteuro in unicode -> tex conversion table
        
        1.0.1 (2010-04-01)
        ==================
        * added version_check API for checking the install Bibutils version
        
        1.0.0 (2010-03-19)
        ==================
        * final release
        
        1.0.0c1 (2010-03-03)
        ====================
        * release candidate
        
        1.0.0b3 (2010-02-01)
        ====================
        - fixed cmdline parameters for ris2bib transformation
        
        1.0.0b2 (2010-01-31)
        ====================
        - added bib2bib transformation
        
        1.0.0b1 (2010-01-28)
        ====================
        - new numbering schema
        - various encoding parameter fixes
        
        0.2.7.1 (2010-01-26)
        ====================
        - minor tweak to ris2bib parameters
        
        0.2.7 (2010-01-22)
        ==================
        - adjust parameters for Endnote conversion
        
        0.2.6 (2010-01-22)
        ==================
        - calling ris2bib  converter with explicit UTF-8 input encoding
        (checked and enforced within bibliograph.parsing)
        
        0.2.5 (2009-12-15)
        ==================
        
        - IBibliographicReference: 'year' is now a TextLine field because
        an 'Int' is to restrictive
        - re-added old mapping unicode chars -> TeX
        
        0.2.4 (2009-12-12)
        ==================
        
        - added publication_month to IBibliographicReference
        
        0.2.3 (2009-12-06)
        ==================
        
        - replaced unicode -> TeX mapping (encodings.py) with a more complete
        mapping as found in docutils.py (unicode_map.py)
        
        0.2.2 (2009-12-04)
        ==================
        
        - IBibliographicReference: added 'identifiers' to schema
        
        
        0.2.1 (2009-08-26)
        ==================
        
        - "_encode"-utility now always returns a string
        
        0.2.0 (2008-09-11)
        ==================
        
        - Moved _getCommand/_hasCommand functions relating to bibutils from bibliograph.rendering to bibliograph.core [tim2p].
        - Updated encodings.py so that accented characters are correctly rendered for BibTeX/LaTeX [tim2p].
        - Removed IBibContainerIterator in favour of rolling the functionality into a (new) IBibliography interface [tim2p].
        - Added an (unused, currently) IAuthors interface that I plan to develop into a part of the author handling api [tim2p].
        - Introduced generic vocabulary `bibliography.formats` for IBibliographyExport interface [tom_gross].
        
        0.1.0 (2008-05-02)
        ==================
        
        - Created recipe with ZopeSkel [tom_gross].
        - Ported necessary core parts from Products.CMFBibliographyAT
        - Added Interfaces
        
        
        
        
Keywords: bibtex bibliography xml endnote ris bibutils
Platform: UNKNOWN
Classifier: Framework :: Plone
Classifier: Framework :: Zope2
Classifier: Framework :: Zope3
Classifier: Programming Language :: Python
Classifier: Development Status :: 5 - Production/Stable
Classifier: Topic :: Software Development :: Libraries :: Python Modules
Classifier: License :: OSI Approved :: Zope Public License
